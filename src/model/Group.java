package model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement	
public class Group {
	private String name;
	private ArrayList<Question> questions;
	
	//public Group() {
	//	questions = new ArrayList<Question>();		
	//}
	
	public Group(String name) {
		this.name = name;		
		questions = new ArrayList<Question>();
	}
	
	public void addQuestion(Question question) {
		// first verify question not already present
		for(Question q : questions) {
			if (q.getId() == question.getId())
				return;			
		}
		questions.add(question);
	}
	
	public ArrayList<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(ArrayList<Question> questions) {
		this.questions = questions;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
