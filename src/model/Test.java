package model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement	
public class Test {
	private String testcode;
	private boolean isprenatal;
	private boolean istrio;
	private ArrayList<String> testtypes = null;
	
	public Test() {
	}
	
	public Test(String testcode, boolean istrio) {
		this.testcode = testcode;		
		this.istrio = istrio;
		this.testtypes = new ArrayList<String>();
	}
	
	public String getTestcode() {
		return testcode;
	}
	public void setTestcode(String testcode) {
		this.testcode = testcode;
	}
	public boolean getIsprenatal() {
		return isprenatal;
	}

	public void setIsprenatal(boolean isprenatal) {
		this.isprenatal = isprenatal;
	}
	
	public boolean getIstrio() {
		return istrio;
	}

	public void setIstrio(boolean istrio) {
		this.istrio = istrio;
	}

	public ArrayList<String> getTesttypes() {
		return testtypes;
	}

	public void setTesttypes(ArrayList<String> testtypes) {
		this.testtypes = testtypes;
	}
	
	public void addTesttype(String type) {
		for(String t : testtypes) {
			if (t.equals(type))
				return;
		}
		testtypes.add(type);
	}
	
}
