package model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement	
public class GetTestCodeQuestionsResponse {
	private ArrayList<Test> tests;
	private ArrayList<Section> sections;

	public GetTestCodeQuestionsResponse() {
		tests = new ArrayList<Test>();
		sections = new ArrayList<Section>();
	}
	
	public void addTestCode(Test testcode) {
		tests.add(testcode);
	}

	public void addSection(Section section) {
		sections.add(section);
	}

	public Section getSection(String sectionName) {
		for(Section s : sections) {
			if (s.getName().equals(sectionName))
				return s;
		}
		
		Section newSection = new Section(sectionName);
		sections.add(newSection);
		return newSection;
	}

	public Group getGroup(String sectionName, String groupName) {
		for(Section s : sections) {
			if (s.getName().equals(sectionName)) {
				return getGroup(s, groupName);
			}
		}
		
		// section does not already exist
		Section newSection = new Section(sectionName);
		sections.add(newSection);
		
		Group newGroup = new Group(groupName);
		newSection.getGroups().add(newGroup);
		
		return newGroup;
	}	
	
	private Group getGroup(Section section, String groupName) {
		for(Group g : section.getGroups()) {
			if (g.getName().equals(groupName))
				return g;
		}
	
		Group newGroup = new Group(groupName);
		section.getGroups().add(newGroup);
		return newGroup;

	}
	
	public ArrayList<Test> getTests() {
		return tests;
	}
	
	public void setTestType(String test, String type) {
		for(Test t : tests) {
			if (t.getTestcode().equals(test)) {
				t.addTesttype(type);
				return;
			}
			
		}
		
		System.out.println("testcode not found:" + test);
	}

	public void setTests(ArrayList<Test> tests) {
		this.tests = tests;
	}
	
	public ArrayList<Section> getSections() {
		return sections;
	}

	public void setSections(ArrayList<Section> sections) {
		this.sections = sections;
	}	
}
