package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement	
public class Question {
	private int id;
	private String label;
	private String type;
	private int required;
	private ArrayList<Choice> options = null;
	private ArrayList<Choice> children = null;

	private String section;	
	private Map<Integer, Choice> choiceMap = null;

	public Question(int id, String section, String type, String category, int required) {
		this.id = id;
		this.section = section;
		this.type = type;
		this.label = category;
		this.required = required; 
		this.options = new ArrayList<Choice>();
		this.children = new ArrayList<Choice>();
		
		this.choiceMap = new HashMap<Integer, Choice>(); 
	}

	public Question() {	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getRequired() {
		return required;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setRequired(int required) {
		this.required = required;
	}

	public ArrayList<Choice> getOptions() {
		return options;
	}

	public void setOptions(ArrayList<Choice> choices) {
		this.options = choices;
	}

	public ArrayList<Choice> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Choice> children) {
		this.children = children;
	}
	
	public void addOption(Choice choice) {
		choiceMap.put(choice.getId(), choice);
		options.add(choice);
	}

	public Choice getChoice(int id) {
		Choice c = choiceMap.get(id);
		if (c == null)
			System.out.println("error: id not found id:" + id);
		
		return c;		
	}
	
	public void add(int parentid, int objectid, Choice c) {
		if (parentid == 0 && objectid==0) {
			addOption(c);
			return;
		}
		
		int id = Math.max(parentid,  objectid);
		Choice parentObject = getChoice(id);		
		if (parentObject != null) {
			choiceMap.put(c.getId(), c);
			if (parentid != 0)
				parentObject.addChild(c);
			else			
				parentObject.addOption(c);
		}
	}

	public void addOption(int choiceid, Choice c) {
		boolean bFound = false;
		for(Choice choice : options) {
			if(choice.getId() == choiceid) {
				choice.addOption(c);
				bFound = true;
			}
		}
		for(Choice choice : children) {
			if(choice.getId() == choiceid) {
				choice.addOption(c);
				bFound = true;
			}
		}
		
		if (!bFound)
			System.out.println("error: choice not found choiceid:" + choiceid + " label:" + c.getLabel());
	}

}

