package model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement	
public class Choice {
	private int id;
	private String label;
	private String type;
	private int required;
	
	private ArrayList<Choice> options = null;
	private ArrayList<Choice> children = null;
	
	public Choice(int id, String label, String type, int required) {
		this.id = id;
		this.label = label;
		this.type = type;
		this.required = required; 
		this.options = new ArrayList<Choice>();
		this.children = new ArrayList<Choice>();
	}	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ArrayList<Choice> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<Choice> options) {
		this.options = options;
	}
	public ArrayList<Choice> getChildren() {
		return children;
	}
	public void setChildren(ArrayList<Choice> children) {
		this.children = children;
	}
	
	public void addOption(Choice choice) {
		options.add(choice);
	}

	public void addChild(Choice choice) {
		children.add(choice);
	}

	public int getRequired() {
		return required;
	}

	public void setRequired(int required) {
		this.required = required;
	}

	
}
