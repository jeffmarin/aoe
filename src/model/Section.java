package model;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement	
public class Section {
	private String name;
	private ArrayList<Group> groups;
	
	public Section() {
		groups = new ArrayList<Group>();		
	}
	
	public Section(String section) {
		this.name = section;		
		groups = new ArrayList<Group>();
	}
	
	public ArrayList<Group> getGroups() {
		return groups;
	}
	public void setGroups(ArrayList<Group> groups) {
		this.groups = groups;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
