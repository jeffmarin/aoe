package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddDefaultConsent {

	static String selectSQL = "select distinct testcode from aoe_testcode where testcode not in ('896', '561', '561a', '561b', '690', '690a', '690b', '959', 'J499', '460')";
	
	static String defaultConsentSQL = "select * from aoe_testcode where testcode = ? and aoe_question_id = 1013";
	
	static String insertSQL = "INSERT INTO aoe_testcode (AOE_Question_ID, TestCode, LabFacilityID, CreatedDate, CreatedBy, Deleted) VALUES(?,?,?,?,?,?)";

	public static void main(String[] args) {
		int count = 0, all = 0;
		Connection con = null; 		
		try {
			con = getConnection();
			ArrayList<String> codes = getUniqueTestCodes(con);
			for(String code : codes){
				all++;
				boolean bExists = checkDefaultConsent(con, code);
				if (!bExists) {
					count++;
					addDefaultConsent(con, code);
				}
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
		
		System.out.println("all:" + all + " count:" + count);
	}

	private static void addDefaultConsent(Connection con, String testcode) {
		System.out.println(testcode);
		PreparedStatement st = null;
        try {
			st = con.prepareStatement(insertSQL);
			st.setInt(1,  1013);
			st.setString(2, testcode);
			st.setInt(3,  1);			
			st.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
			st.setString(5, "jmarin");
			st.setInt(6,  0);			
			st.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }         
	}	
	
	
	private static boolean checkDefaultConsent(Connection con, String testcode) {
		PreparedStatement st = null;
		boolean bExists = false;
        ResultSet rs = null;
		
        try {
			st = con.prepareStatement(defaultConsentSQL);
			st.setString(1,  testcode);
			
			rs = st.executeQuery();
			if (rs.next())
				bExists = true; 
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }   
        return bExists;
	}	
	
	private static ArrayList<String> getUniqueTestCodes(Connection con) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        ArrayList<String> codes = new ArrayList<String>(); 
        try {
			st = con.prepareStatement(selectSQL);
			
			rs = st.executeQuery();
			while (rs.next()) {
				String testcode = rs.getString(1);
				codes.add(testcode);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }   
        return codes;
	}	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
