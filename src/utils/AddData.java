package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddData {

	static String selectSQL = "select distinct testcode from aoe_testcode where testcode not in ('896', '561', '561a', '561b', '690', '690a', '690b', '959', 'J499', '460')";

	static String insertSQL = "INSERT INTO aoe_testcode (AOE_Question_ID, TestCode, LabFacilityID, CreatedDate, CreatedBy, Deleted) VALUES(?,?,?,?,?,?)";
	
	static String testcodes = "561,561a,561b,690,690a,690b,959,J499,460";

	public static void main(String args[]) {
		Connection con = null; 		
		try {
			con = getConnection();
			ArrayList<String> codes = getUniqueTestCodes(con);
			for(String code : codes){
				addTestCodeQuestions(con, code, 1013);

			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}
	
	private static ArrayList<String> getUniqueTestCodes(Connection con) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        ArrayList<String> codes = new ArrayList<String>(); 
        try {
			st = con.prepareStatement(selectSQL);
			
			rs = st.executeQuery();
			while (rs.next()) {
				String testcode = rs.getString(1);
				codes.add(testcode);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }   
        return codes;
	}	
	
	private static void addTestCodeQuestions(Connection con, String testcode, int questionID) {
		PreparedStatement st = null;
        try {
			st = con.prepareStatement(insertSQL);
			st.setInt(1,  questionID);
			st.setString(2, testcode);
			st.setInt(3,  1);			
			st.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
			st.setString(5, "jmarin");
			st.setInt(6,  0);			
			st.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }         
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
