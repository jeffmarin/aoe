package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AddNeuroQuestionsFromFile {
	
	static String testcodes = "910,905,J542,4343,4345,336";

	static String insertSQL = "INSERT INTO aoe_testcode (AOE_Question_ID, TestCode, LabFacilityID, CreatedDate, CreatedBy, Deleted) VALUES(?,?,?,?,?,?)";
	
	static String insertQuestionSQL = "INSERT INTO questions (id, category, section) VALUES(?,?,'CI')";
	
	static String insertChoiceSQL = "INSERT INTO choices (questionid, label, optionid, parentid) VALUES(?,?,?,?)";

	static String cardioQuestionsFile = "C:\\Users\\Administrator\\Downloads\\Neuro_questions.txt";
	
	public static void main(String[] args) {
		new AddNeuroQuestionsFromFile();

	}

	
	public AddNeuroQuestionsFromFile() {
		Connection con = null; 		
		try {
			con = getConnection();
			
			int nextQuestionID = 1100;
			
			int currentQuestionId = 0, currentChoiceId = 0;
			File file = new File(cardioQuestionsFile);
			try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			    for(String line; (line = br.readLine()) != null; ) {
			    	if (line.length() == 0)
			    		continue;
			    	
			    	if (line.startsWith("_"))
			    		continue;
			    	
			    	if (line.startsWith("r ")) {
			    		// create choice
			    		currentChoiceId = createChoice(con, currentQuestionId, line.substring(2).trim(), 0, 0);
			    	} else if (line.startsWith(" r ")) {
			    		// create child
			    		createChoice(con, currentQuestionId, line.substring(2).trim(), 0, currentChoiceId);
			    	} else {
			    		// create question
			    		currentQuestionId = createQuestion(con, nextQuestionID, line.trim());
			    		currentChoiceId = 0;
			    		nextQuestionID++;
			    	}
			    	
			    	if (line.contains("__")) {
			    		// create option
			    		createChoice(con, currentQuestionId, "", currentChoiceId, 0);
			    	}
			    	
			    
			    	
			        System.out.println(line);
			    }
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			/*
			
			String codes[] = testcodes.split(",");
			for(String code : codes){
				System.out.println(code);
				addQuestions(con, code);
			}
			*/
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 				
	}
	
	private static int createQuestion(Connection con, int questionID, String category) {
		int ret = 0;
		
		PreparedStatement st = null;
        try {
			st = con.prepareStatement(insertQuestionSQL, Statement.RETURN_GENERATED_KEYS);
			st.setInt(1,  questionID);
			st.setString(2, category);
			int affectedrows = st.executeUpdate();
			
			try (ResultSet generatedKeys = st.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					ret = generatedKeys.getInt(1);
				}				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
        
        return ret;
	}
	
	private static int createChoice(Connection con, int questionid, String label, int optionid, int parentid) {
		int ret = 0;
		
		PreparedStatement st = null;
        try {
			st = con.prepareStatement(insertChoiceSQL, Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, questionid);
			st.setString(2, label);
			st.setInt(3, optionid);
			st.setInt(4, parentid);
			st.executeUpdate();		
			
			try (ResultSet generatedKeys = st.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					ret = generatedKeys.getInt(1);
				}				
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
        
        return ret;
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
