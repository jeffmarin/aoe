package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RemoveSIQuestions {
	
	static String questioncodes = "894,924,955,973,975,1027,1028,1029,1030,1031,1032,1033,1034,1035,1036,1037,1038,1039,1040";

	static String deleteAnswersSQL = "DELETE FROM aoe_answers WHERE AOE_Question_ID = ?";
	static String deleteQuestionSQL = "DELETE FROM aoe_questions WHERE AOE_Question_ID = ?";
	static String deleteTestCodeQuestionSQL = "DELETE FROM aoe_testcode WHERE AOE_Question_ID = ?";

	public static void main(String[] args) {
		Connection con = null; 		
		try {
			con = getConnection();
			String codes[] = questioncodes.split(",");
			for(String code : codes){
				System.out.println(code);
				removeAnswers(con, Integer.parseInt(code));
				removeTestCodeQuestions(con, Integer.parseInt(code));
				removeQuestion(con, Integer.parseInt(code));
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}
	
	private static void removeQuestion(Connection con, int qid) {
		PreparedStatement st = null;
	    try {
			st = con.prepareStatement(deleteQuestionSQL);
			st.setInt(1,  qid);
			st.execute();			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
	}	
	
	private static void removeTestCodeQuestions(Connection con, int qid) {
		PreparedStatement st = null;
	    try {
			st = con.prepareStatement(deleteTestCodeQuestionSQL);
			st.setInt(1,  qid);
			st.execute();			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
	}	

	private static void removeAnswers(Connection con, int qid) {
		PreparedStatement st = null;
	    try {
			st = con.prepareStatement(deleteAnswersSQL);
			st.setInt(1,  qid);
			st.execute();			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
