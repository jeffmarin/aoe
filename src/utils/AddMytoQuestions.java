package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddMytoQuestions {
	
	static String testcodes = "910,905,J542,4343,4345,336";

	static String insertSQL = "INSERT INTO aoe_testcode (AOE_Question_ID, TestCode, LabFacilityID, CreatedDate, CreatedBy, Deleted) VALUES(?,?,?,?,?,?)";

	public static void main(String[] args) {
		Connection con = null; 		
		try {
			con = getConnection();
			String codes[] = testcodes.split(",");
			for(String code : codes){
				System.out.println(code);
				addQuestions(con, code);
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}

	private static void addQuestions(Connection con, String testcode) {
		for(int qid = 1014; qid <= 1026; qid++) {
			System.out.println(testcode);
			PreparedStatement st = null;
	        try {
				st = con.prepareStatement(insertSQL);
				st.setInt(1,  qid);
				st.setString(2, testcode);
				st.setInt(3,  1);			
				st.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
				st.setString(5, "jmarin");
				st.setInt(6,  0);			
				st.execute();			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
		        try {
		            if (st != null) {
		                st.close();
		            }
		        } catch (SQLException ex) {
		        	System.out.println(ex.getMessage());
		        }
		    }
		}
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
