package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddPostnatalQuestionsToTestCodes {
	
	static String selectSQL = "select distinct testcode from aoe_testcode where testcode not in ('1001','357','907','2201','409','702','746','937','949','738','2523','408','2363','407','3383','428','2373','934','2262','2503','2553','2553E','663','433','410','460','J542','410','460','455','4341','4342','4346','4344','4343','4345','1122-1','1952-1','437') order by testcode";

	static String insertSQL = "INSERT INTO aoe_testcode (AOE_Question_ID, TestCode, LabFacilityID, CreatedDate, CreatedBy, Deleted) VALUES(?,?,?,?,?,?)";

	public static void main(String[] args) {
		Connection con = null; 		
		try {
			con = getConnection();
			ArrayList<String> codes = getUniqueTestCodes(con);
			for(String code : codes){
				System.out.println(code);
				addQuestions(con, code);
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}

	private static ArrayList<String> getUniqueTestCodes(Connection con) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        ArrayList<String> codes = new ArrayList<String>(); 
        try {
			st = con.prepareStatement(selectSQL);
			
			rs = st.executeQuery();
			while (rs.next()) {
				String testcode = rs.getString(1);
				codes.add(testcode);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }   
        return codes;
	}
	
	private static void addQuestions(Connection con, String testcode) {
		for(int qid = 1033; qid <= 1040; qid++) {
			PreparedStatement st = null;
	        try {
				st = con.prepareStatement(insertSQL);
				st.setInt(1,  qid);
				st.setString(2, testcode);
				st.setInt(3,  1);			
				st.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
				st.setString(5, "jmarin");
				st.setInt(6,  0);			
				st.execute();			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
		        try {
		            if (st != null) {
		                st.close();
		            }
		        } catch (SQLException ex) {
		        	System.out.println(ex.getMessage());
		        }
		    }
		}
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
