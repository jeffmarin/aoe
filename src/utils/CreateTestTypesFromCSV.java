package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTestTypesFromCSV {
	
	static String insertCategorySQL = "INSERT INTO testcategory (testcode, category) VALUES(?,?)";

	static String typesFile = "C:\\Users\\Administrator\\Downloads\\Clinical-Info-Full-List_LenaEdits.csv";
	
	public static void main(String[] args) {
		new CreateTestTypesFromCSV();
	}
	
	public CreateTestTypesFromCSV() {
		Connection con = null; 		
		try {
			con = getConnection();
			
			File file = new File(typesFile);
			try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			    for(String line; (line = br.readLine()) != null; ) {
			    	if (line.length() == 0)
			    		continue;
			    	
			    	String words[] = line.split(",");
			    	if (words.length >=  2) {			    	
			    		createTestCategory(con, words[0], words[1]);
			    	} else {
			    		System.out.println("Only " + words.length + " words");
			    	}
			    	
			    	
			        //System.out.println(line);
			    }
			} catch (Exception e) {
				e.printStackTrace();
			}			
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 				
	}
	
	private static int createTestCategory(Connection con, String testcode, String category) {
		System.out.println(testcode + " " + category);

		int ret = 0;
		
		PreparedStatement st = null;
        try {
			st = con.prepareStatement(insertCategorySQL, Statement.RETURN_GENERATED_KEYS);
			st.setString(1, testcode);
			st.setString(2, category);
			int affectedrows = st.executeUpdate();
			
			try (ResultSet generatedKeys = st.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					ret = generatedKeys.getInt(1);
				}				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
        
        return ret;
	}
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
