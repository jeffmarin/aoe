package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddQuestionsToTestCodes {
	
	static String[] testcodes = {"J894a","J894b","J905a","J905b"}; 

	static int[] questionIds = new int[]{	
	1100,1101,1102,1103,1104,1106,1107,1108,1109,1110,1111,
	1112,1113,1114,1115,1116,1117,1118,1119,1300,1301,1302,
	1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,
	1314,1315,1316,1317,1012,1013};
	
	static String insertSQL = "INSERT INTO testcode (testcode, questionid) VALUES(?,?)";
	

	public static void main(String[] args) {
		Connection con = null; 		
		try {
			con = getConnection();
			for(String testcode : testcodes){
				for(int questionId : questionIds){
					System.out.println(testcode + ',' + questionId);
					addQuestion(con, testcode, questionId);
				}
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}

	private static void addQuestion(Connection con, String testcode, int questionId) {
		PreparedStatement st = null;
        try {
			st = con.prepareStatement(insertSQL);
			st.setString(1, testcode);
			st.setInt(2,  questionId);
			st.execute();			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
