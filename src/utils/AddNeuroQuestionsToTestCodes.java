package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddNeuroQuestionsToTestCodes {
	
	static String testcodes = "690a,690b,690c,561a,561b,561c"; 

	static String insertSQL = "INSERT INTO testcode (questionid, testcode) VALUES(?,?)";
	
	static String neuroTestFile = "C:\\Users\\Administrator\\Downloads\\Neuro_testcodes.txt";

	public static void main(String[] args) {
		Connection con = null; 		
		try {
			con = getConnection();
			String codes[] = testcodes.split(",");
			for(String code : codes){
				System.out.println(code);
				addQuestions(con, code);
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}
	
	private static void getTestCodesFromFile() {
		try {
			File file = new File(neuroTestFile);
			BufferedReader br = new BufferedReader(new FileReader(file));
		    for(String line; (line = br.readLine()) != null; ) {
		    	int pos = line.indexOf(" ");
		    	String test = line.substring(0, pos);
		    	System.out.print(test);
		    	System.out.print(",");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private static void addQuestions(Connection con, String testcode) {
		for(int qid = 1100; qid <= 1119; qid++) {
			if (qid != 1105) {
				System.out.println(testcode);
				PreparedStatement st = null;
		        try {
					st = con.prepareStatement(insertSQL);
					st.setInt(1,  qid);
					st.setString(2, testcode);
					st.execute();			
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
			        try {
			            if (st != null) {
			                st.close();
			            }
			        } catch (SQLException ex) {
			        	System.out.println(ex.getMessage());
			        }
			    }
			}
		}
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
