package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddPrenatalQuestionsToTestCodes {
	
	static String testcodes = "433,410,460,J542,455,4341,4342,4346,4344,4343,4345,1122-1,437,357,907,2201,409,702,746,937,949,738,2523,408,2363,407,3383,428,2373,934,2262,2503,2553,2553E,663"; 

	static String insertSQL = "INSERT INTO testcode (questionid, testcode) VALUES(?,?)";
	
	static String prenatalTestFile = "C:\\Users\\Administrator\\Downloads\\prenatal_tests.txt";

	public static void main(String[] args) {
		Connection con = null; 		
		try {
			con = getConnection();
			String codes[] = testcodes.split(",");
			for(String code : codes){
				System.out.println(code);
				addQuestions(con, code);
			}
		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    } 
	}
	
	private static void getTestCodesFromFile() {
		try {
			File file = new File(prenatalTestFile);
			BufferedReader br = new BufferedReader(new FileReader(file));
		    for(String line; (line = br.readLine()) != null; ) {
		    	int pos = line.indexOf(" ");
		    	String test = line.substring(0, pos);
		    	System.out.print(test);
		    	System.out.print(",");
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private static void addQuestions(Connection con, String testcode) {
		for(int qid = 1400; qid <= 1410; qid++) {
			System.out.println(testcode);
			PreparedStatement st = null;
	        try {
				st = con.prepareStatement(insertSQL);
				st.setInt(1,  qid);
				st.setString(2, testcode);
				st.execute();			
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
		        try {
		            if (st != null) {
		                st.close();
		            }
		        } catch (SQLException ex) {
		        	System.out.println(ex.getMessage());
		        }
		    }
		}
	}	
	
	private static Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}	
}
