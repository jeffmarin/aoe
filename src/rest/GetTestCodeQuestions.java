package rest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import model.Choice;
import model.GetTestCodeQuestionsResponse;
import model.Group;
import model.Question;
import model.Test;
import utils.DataSource;

@Path("/get")
public class GetTestCodeQuestions {
	private Log log = LogFactory.getLog(GetTestCodeQuestions.class);

	private static String questionsSQL = "SELECT id, section, label, required, category, type FROM questions where id in "
			+ "(SELECT DISTINCT questionid FROM testcode where testcode IN (?))";

	private static String questionsByCategorySQL = "SELECT id, section, label, required, category, type FROM questions where category = ?";

	private static String consentQuestionsSQL = "SELECT id, section, label, required, category, type FROM questions where id IN (1012, 1013)";

	private static String XomeConsentQuestionsSQL = "SELECT id, section, label, required, category, type FROM questions where id IN (1012, 1500)";

	private static String J775ConsentQuestionsSQL = "SELECT id, section, label, required, category, type FROM questions where id IN (1012, 1501)";

	private static String choicesSQL = "SELECT id, label, type, required, parentid, optionid FROM choices where questionid = ? ORDER BY id";
	
	private static String preNatalCodes = ",357,907,2201,409,702,746,937,949,738,2523,408,2363,407,3383,428,2373,934,2262,2503,2553,2553E,663,433,410,460,J542,"
			+ "410,460,455,4341,4342,4346,4344,4345,1122-1,1952-1,437,902a,959,J499,";
	
	private static String istrioCodes = ",561a,690a,896,921,952,J511,J762,J499,J757,";
	
	private static String isXomeCodes = ",896,561,561a,561b,690,690a,690b,959,J499,460,J774,706,J757,";
		
	@Context private HttpServletResponse response;
	@GET
	@Produces("application/json")
	public Response getQuestions(@QueryParam("testcodes") String testcodes) {
		GetTestCodeQuestionsResponse g = new GetTestCodeQuestionsResponse();
		
		ArrayList<Question> questions = new ArrayList<Question>();
		
		Connection con = null; 		
		try {
			con = getConnection();
			
			getTests(g, con, testcodes);
			
			for (Test test:g.getTests())
				for(String type : test.getTesttypes())
					getCategoryQuestions(g, con, type, questions);
			
			getConsentQuestions(g, con, questions, testcodes);
			
	        for(Question question : questions) {		
	        	getChoices(con, question);
	        }

		} finally {
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }     
		
	    return Response
	            .status(200)
	            .header("Access-Control-Allow-Origin", "*")
	            .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
	            .header("Access-Control-Allow-Credentials", "true")
	            .header("Access-Control-Allow-Methods", "GET")
	            .header("Access-Control-Max-Age", "1209600")
	            .entity(g)
	            .build();	    
	}
	
	private void getTests(GetTestCodeQuestionsResponse g, Connection con, String testCodes) {
		String codes[] = testCodes.split(",");
		for(String code : codes){				
			boolean isPrenatal = preNatalCodes.indexOf("," + code + ",") != -1;
			boolean istrio = istrioCodes.indexOf("," + code + ",") != -1;
			
			Test t = new Test(code, false);
			t.setIsprenatal(isPrenatal);
			t.setIstrio(istrio);
			g.addTestCode(t);
		}	
		
		getTestCategories(g, con, testCodes);
	}
	
	private void getTestCategories(GetTestCodeQuestionsResponse g, Connection con, String testCodes) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT testcode, category FROM testcategory t WHERE testcode IN (");
                
        String testCodeArray[] = testCodes.split(",");
        for(String testcode : testCodeArray)
        	sql.append("?,");
        	
        try {
        	String actual = sql.substring(0, sql.length()-1) + ") ORDER BY testcode";
			st = con.prepareStatement(actual);

			int count = 0;
	        for(String testcode : testCodeArray)
	        	st.setString(++count,testcode);

			rs = st.executeQuery();
			while (rs.next()) {
				String test = rs.getString(1);
				String category = rs.getString(2);				
				g.setTestType(test, category);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }         
	}	
	
	private void getTestCodeQuestions(GetTestCodeQuestionsResponse g, Connection con, String testcode, ArrayList<Question> questions) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        try {
			st = con.prepareStatement(questionsSQL);
			st.setString(1, testcode);
			
			rs = st.executeQuery();
			while (rs.next()) {
				int questionID = rs.getInt(1);
				String section = rs.getString(2);
				String label = rs.getString(3);
				int required = rs.getInt(4);
				String category = rs.getString(5);
				String type = rs.getString(6);
				
				Question q = new Question(questionID, section, type, label, required);
				questions.add(q);				
				
				Group group = g.getGroup(section, category);
				group.addQuestion(q);	
				
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }         
	}

	private void getCategoryQuestions(GetTestCodeQuestionsResponse g, Connection con, String queryCategory, ArrayList<Question> questions) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        try {
			st = con.prepareStatement(questionsByCategorySQL);
			st.setString(1, queryCategory);
			
			rs = st.executeQuery();
			while (rs.next()) {
				int questionID = rs.getInt(1);
				String section = rs.getString(2);
				String label = rs.getString(3);
				int required = rs.getInt(4);
				String category = rs.getString(5);
				String type = rs.getString(6);
				
				Question q = new Question(questionID, section, type, label, required);
				questions.add(q);				
				
				Group group = g.getGroup(section, category);
				group.addQuestion(q);	
				
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }         
	}
	
	private void getConsentQuestions(GetTestCodeQuestionsResponse g, Connection con, ArrayList<Question> questions, String testCodes) {
		boolean isXome = false, isNotXome = false, isJ775 = false;
		
		String codes[] = testCodes.split(",");
		for(String code : codes){
			if (!isXome)
				isXome = isXomeCodes.indexOf("," + code + ",") != -1;
			
			if (!isNotXome)
				isNotXome = isXomeCodes.indexOf("," + code + ",") == -1;			

			if (!isJ775)
				isJ775 = (code.equals("J775")); 			
		}	
		
		if (isNotXome && !isJ775)
			addConsentQuestions(g, con, questions, consentQuestionsSQL);
		
		if (isXome)
			addConsentQuestions(g, con, questions, XomeConsentQuestionsSQL);
		
		if (isJ775)
			addConsentQuestions(g, con, questions, J775ConsentQuestionsSQL);  				
		
	}

	private void addConsentQuestions(GetTestCodeQuestionsResponse g, Connection con, ArrayList<Question> questions, String sql) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        try {
			st = con.prepareStatement(sql);
			
			rs = st.executeQuery();
			while (rs.next()) {
				int questionID = rs.getInt(1);
				String section = rs.getString(2);
				String label = rs.getString(3);
				int required = rs.getInt(4);
				String category = rs.getString(5);
				String type = rs.getString(6);
				
				Question q = new Question(questionID, section, type, label, required);
				questions.add(q);				
				
				Group group = g.getGroup(section, category);
				group.addQuestion(q);					
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }
	}
	
	private void getChoices(Connection con, Question question) {
		PreparedStatement st = null;
        ResultSet rs = null;
		
        try {
			st = con.prepareStatement(choicesSQL);
			st.setInt(1, question.getId());
			
			if (question.getId() == 1028) {
				int qq =0;
			}
			
			rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String label = rs.getString(2);
				String type = rs.getString(3);
				int required = rs.getInt(4);
				Choice choice = new Choice(id, label, type, required);				
				
				int parentid = rs.getInt(5);
				int optionid = rs.getInt(6);

				question.add(parentid, optionid, choice);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException ex) {
	        	System.out.println(ex.getMessage());
	        }
	    }                
	}
	
	private Connection getConnection() {
		Connection connection = null;
        try {
            connection = DataSource.getInstance().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
		return connection;
	}
}
